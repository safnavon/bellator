'use strict';

/**
 * @ngdoc overview
 * @name Bellator
 * @description
 * # bellatorApp
 *
 * Main module of the application.
 */
angular
  .module('Bellator', [
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/CompetitiveEvent.html',
        controller: 'competitiveEventCtrl',
        controllerAs: 'competitiveEventCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
      })
      .when('/history', {
        templateUrl: 'views/FightersHistory.html',
        controller: 'fightersHistoryCtrl',
        controllerAs: 'fightersHistoryCtrl'
      })
      .when('/roadblock', {
        templateUrl: 'views/RoadBlock.html',
        controller: 'roadblockCtrl',
        controllerAs: 'roadblockCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });

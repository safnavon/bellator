﻿
const dataRoute = "../../data/";


angular.module('Bellator').service('BellatorService', function ($http) {


  this.getFighterStats = function (fighter_id) {
    return $http.get(dataRoute + 'Home/GetFighterStats?fighter_id=' + fighter_id);
  };
  this.getCompetitiveEventData = function (event_id, event_type) {
    return $http.get(dataRoute + 'CompetitiveEvents/' + event_id + "-" + event_type + ".json");
  };
  this.getFighterHistory = function (fighter_id) {
    return $http.get(dataRoute + 'FightersHistory/' + fighter_id + ".json");
  };

  this.getFighterImageUrl = function (fighter_id, result) {
    var url = 'http://res.cloudinary.com/ananey/image/upload/c_scale,w_150/c_scale,g_south,l_'
      +fighter_id+',w_150/v1493888945/eventpage-fighter_'+event.contestants[1].result.position
      switch (event.contestants[1].result.position){
        case "blue":
          url+='_bg_jbqtgo'
              break;
        case "red":
          url+='_bg_fl2nlm'
              break;
        case "grey":
          url+=''
      }
      url+='.png'

    return url;
  };

});

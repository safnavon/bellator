'use strict';

/**
 * @ngdoc function
 * @name bellatorApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the bellatorApp
 */
angular.module('Bellator')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
